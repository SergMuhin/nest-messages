# Nest Messages

Nest Messages is a simple project that demonstrates file manipulation with messages using Nest.js.

## API Endpoints

- `GET /api/messages`: Retrieves all messages.
- `GET /api/messages/:id`: Retrieves a specific message by ID.
- `POST /api/messages`: Creates a new message with the following JSON body:
  ```json
  {
    "content": "this is message"
  }
  ```

## Getting Started

To get started with Nest Messages, follow these steps:

1. **Clone the repository**:
   ```
   git clone https://gitlab.com/SergMuhin/nest-messages.git
   ```

2. **Install dependencies**:
   ```
   cd nest-messages
   npm install
   ```

3. **Set up environment variables**:
   Create a `.env` file in the root directory and configure any necessary environment variables.

4. **Run the application**:
   ```
   npm start
   ```

## Contributing

Contributions are welcome! If you'd like to contribute to Nest Messages, please follow these steps:

1. Fork the repository.
2. Create your feature branch (`git checkout -b feature/YourFeature`).
3. Commit your changes (`git commit -am 'Add some feature'`).
4. Push to the branch (`git push origin feature/YourFeature`).
5. Open a pull request.
